---
title: "OpenPGP Replacement Key Signalling Mechanism"
category: info
docname: draft-gallagher-openpgp-replacementkey-01
updates: 4880
ipr: trust200902
area: int
workgroup: openpgp
keyword: Internet-Draft
submissionType: IETF
venue:
  group: "OpenPGP"
  type: "Working Group"
  mail: "openpgp@ietf.org"
  arch: "https://mailarchive.ietf.org/arch/browse/openpgp/"
  repo: "https://gitlab.com/andrewgdotcom/draft-gallagher-openpgp-replacementkey"
  latest: "https://andrewgdotcom.gitlab.io/draft-gallagher-openpgp-replacementkey"
stand_alone: yes
pi: [toc, sortrefs, symrefs]
author:
 -
    fullname: Daphne Shaw
    organization: Jabberwocky Tech
    email: dshaw@jabberwocky.com
 -
    fullname: Andrew Gallagher
    organization: PGPKeys.EU
    email: andrewg@andrewg.com
    role: editor
normative:
  crypto-refresh:
    target: https://datatracker.ietf.org/doc/html/draft-ietf-openpgp-crypto-refresh-13
    title: "OpenPGP"
    author:
      -
        name: Paul Wouters
      -
        name: Daniel Huigens
      -
        name: Justus Winter
      -
        name: Niibe Yutaka
    date: October 2023
informative:

--- abstract

This document specifies a method in OpenPGP to suggest a replacement for an expired or revoked key.

--- middle

# Introduction {#introduction}

The OpenPGP message format [crypto-refresh] defines two ways to invalidate a key.
One way is that the key may be explicitly revoked via a revocation signature.
OpenPGP also supports the concept of key expiration, a date after which the key should not be used.
When a key is revoked or expires, very often there is another key that is intended to replace it.

This document is to specify the format of a signature subpacket to be optionally included in the revocation signature or in the self-signature for a key that has an expiration date.
This subpacket contains a pointer to a suggested replacement for the revoked or expired key.

# Conventions and Definitions {#conventions-definitions}

{::boilerplate bcp14-tagged}

# The Replacement Key Subpacket {#replacement-key-subpacket}

 The replacement key subpacket is a Signature Subpacket as defined in [crypto-refresh], section 5.2.3.7, and all general signature subpacket considerations from there apply here as well.
 The value of the signature subpacket type octet for the replacement key subpacket is (insert this later).

A preferred key server subpacket ([crypto-refresh], section 5.2.3.26) MAY be included in the revocation or self-signature to recommend a location and method to fetch the replacement key.

The absence of a replacement key subpacket SHOULD NOT be interpreted as meaning that there is no replacement for a revoked or expired key.

The replacement key subpacket is only meaningful in a key revocation or self-signature.
It SHOULD NOT be present in any other sort of signature.

# Format of the Replacement Key Subpacket {#replacement-key-subpacket-format}

The format of the replacement key subpacket is 1 octet of subpacket version and 1 octet of class, followed by an optional 1 octet of key version and N octets of fingerprint.

The subpacket version octet MUST be set to 0x01 to indicate the version of the replacement key subpacket as specified in this document.
An implementation that encounters a subpacket version octet that is different than the version(s) it is capable of understanding MUST disregard that replacement key subpacket.
Note that if the critical bit for the replacement key subpacket is set, this MAY also mean considering the whole signature to be in error ([crypto-refresh], section 5.2.3.7).

The 0x80 bit of the class octet is the "no replacement" bit.
When set, this explicitly specifies there is no replacement for the revoked or expired key.
All other bits of the class octet are currently undefined and MUST be set to zero.

If the class octet does not have the 0x80 bit set to indicate there is no replacement, the replacement key subpacket also contains 1 octet for the key version of the replacement key and N octets for the fingerprint of the replacement key.
If present, the length of the fingerprint field MUST equal the fingerprint length corresponding to the key version field, e.g.
20 octets for version 4, or 32 octets for version 6.

If the intent is to state that the replacement key is unknown, then no replacement key subpacket should be included in the revocation signature.

If multiple replacement key subpackets are present, implementations MAY use any method desired to resolve which key (or keys) are the chosen replacement.

# Trust and Validation of the Replacement Key Subpacket {#replacement-key-subpacket-trust}

No unusual trust in the replacement key should be implied by it being designated as the replacement.
Implementations SHOULD validate the replacement key as they would any other key.

# Placement of the Replacement Key Subpacket {#replacement-key-subpacket-placement}

While nothing prevents using the replacement key subpacket on a subkey revocation or self-signature, it is mainly useful on a primary key revocation or self-signature as a replacement subkey can be directly added by the keyholder with no need for the indirection provided by this subpacket.
The replacement key subpacket SHOULD be placed in the hashed section of the signature to prevent a possible key substitution attack.
If the replacement key subpacket was allowed in the unhashed section of the signature, an attacker could add a bogus replacement key subpacket to an existing revocation or self-signature.

# Security Considerations {#security-considerations}

The replacement key subpacket provides non-sensitive information only.
Nevertheless, as noted above, implementations SHOULD NOT trust a replacement key subpacket that is located in the unhashed area of the signature packet, and SHOULD validate the replacement key as they would any other key.
In addition, as this document is an update of [crypto-refresh], the security considerations there should be carefully reviewed.

# IANA Considerations {#iana-considerations}

This document requests that the following entry be added to the OpenPGP Signature Subpacket registry:

{: title="Signature Subpacket Registry" #signature-subpacket-registry}
Type | Name               | Specification
-----|--------------------|-------------------------
TBC  | Replacement Key    | This document

--- back

# Acknowledgments

The authors would like to thank Aron Wussler for his contributions.

# Document History

Note to RFC Editor: this section should be removed before publication.

## Changes Between -00 and -01

## Changes Between draft-shaw-openpgp-replacementkey-00 and draft-gallagher-openpgp-replacementkey-00

* Changed `algid` octet to `key version` octet.
* Changed initial subpacket version number to 1.
* Clarified semantics of some edge cases.
