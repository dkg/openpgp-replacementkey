# OpenPGP Replacement Key Signalling Mechanism

This repository holds the source code for the [OpenPGP Replacement Key Signalling Mechanism Internet Draft](https://datatracker.ietf.org/doc/draft-gallagher-openpgp-replacementkey/).
This is a direct successor of, and is directly based upon, the previous [Key Replacement for Revoked Keys in OpenPGP Internet Draft](https://datatracker.ietf.org/doc/html/draft-shaw-openpgp-replacementkey-00).
Merge requests are welcome.

## List of documents

Included in this repository are the following documents:

* `replacementkey.md`
    Markdown source for the OpenPGP Replacement Key Signalling Mechanism Internet Draft.
